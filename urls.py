from django.conf.urls import patterns, include, url
from django.conf import settings

urlpatterns = patterns('',
    url(r'^local2/', include('local2dj14.urls')),
    # Examples:
    # url(r'^$', 'local2django17bench.views.home', name='home'),
    # url(r'^local2django17bench/', include('local2django17bench.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
