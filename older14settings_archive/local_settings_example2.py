# Django local_settings for local2django14bench project.
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

SECRET_KEY = 'placeholder'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        #'TIMEOUT': 60,
        #'OPTIONS': {
        #    'MAX_ENTRIES': 1000
        #},
        #'KEY_PREFIX': '',
        #'LOCATION': [
        #    '111.222.111.222:11211',
        #    '111.222.111.233:11211',
        #    '111.222.111.244:11214',
        #],
    }
}


