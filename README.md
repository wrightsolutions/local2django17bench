# Django benchmarking

## Django settings files and piecemeal construction

The main settings py is [by convention] settings.py

A file labelled settings_langplus.py if used is usually imported at the TOP of settings.py

A file labelled settings_final.py if used is usually imported at the TAIL of settings.py

final is the file where you should put last minute or environment overrides that are not too
numerous. Certainly 'final' should have the final word on DEBUG and LOGGING definitions.


## Virtual hosts, apache 2.4 and cloud servers

Do use the -S flag to check your * in VirtualHost really is picking up all the proper IP addresses

Cloud folks often forget or overlook the definition of the external address and you end up with this 127.0.1.1 limitation!!

apache2ctl -S
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the 'ServerName' directive globally to suppress this message
VirtualHost configuration:
*:80                   127.0.1.1 (/etc/apache2/sites-enabled/000-default.conf:1)
*:1780                 127.0.1.1 (/etc/apache2/sites-enabled/local2dj14.conf:2)
ServerRoot: "/etc/apache2"
Main DocumentRoot: "/var/www/html"
Main ErrorLog: "/var/log/apache2/error.log"
Mutex default: dir="/var/lock/apache2" mechanism=fcntl 
Mutex watchdog-callback: using_defaults
PidFile: "/var/run/apache2/apache2.pid"
Define: DUMP_VHOSTS
Define: DUMP_RUN_CFG
User: name="www-data" id=33
Group: name="www-data" id=33