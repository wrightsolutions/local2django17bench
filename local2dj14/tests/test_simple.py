from django.test import TestCase

import time

URL_COMMON0='local2'
URL_COMMON1='/local2'

class SourceWithTest(TestCase):

    def test_with(self,source_query=None,minimum_results=5):
        if source_query is None:
            return

        test_target = "/{0}/simplelist/{1}/{2}/".format(URL_COMMON0
                                                       ,int(time.time()),'6775471')
        #print test_target
        # POST request to simplelist
        resp = self.client.post(test_target,{'source_query': source_query},follow=False)
        if resp.status_code != 200:
            print resp.status_code
            raise Exception(resp)

        self.assertEqual(resp.status_code, 200)
        self.assertTrue('list_of_dicts' in resp.context)
        #print resp.context['title']
        list_of_dicts = resp.context['list_of_dicts']
        len_list_of_dicts = len(list_of_dicts)
        #self.assertEqual(len_list_of_dicts,0,'No results should have been returned')
        self.assertTrue(len_list_of_dicts >= minimum_results,
                        "At least {0} results should have been returned but {1} {2}".format(
                minimum_results,len_list_of_dicts,'falls short.'))


class SourceWithAll(SourceWithTest):

    def test_with_alone(self):
        self.test_with('with',10)

    def test_with_open(self):
        self.test_with('with open',6)

    def test_with_open_singles(self):
        self.test_with("'with open'",0)

    def test_with_open_doubles(self):
        self.test_with('"with open"',3)

