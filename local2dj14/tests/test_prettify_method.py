from django.test import TestCase
from local2dj14.models import Source, Def

URL_COMMON0='local2'
URL_COMMON1='/local2'

class PrettifiedMethod(TestCase):

    #fixtures = ['local_hyphen_source_def']

    def test_prettify_method(self,name=None,prettified_count=0,title_includes=None):
        if name is None:
            return
        """Tests that counts url completes okay and was supplied with
        expected entry in context.
        """

        test_target = "/{0}/prettify/local-database/rc/py/{1}/".format(URL_COMMON0,
                                                        name)
        #print name, test_target
        resp = self.client.get(test_target)
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('def_prettified_count' in resp.context)
        def_prettified_count = resp.context['def_prettified_count']
        self.assertEqual(def_prettified_count, prettified_count)

        self.assertTrue('title' in resp.context)
        if title_includes:
            title = resp.context['title']
            self.assertTrue(title_includes in title,
                            'Expected title to contain {0}'.format(title_includes))
            
        """
        if resp.status_code != 200:
            print resp.status_code
            raise Exception(resp)
        """

     
class SingleMultiplePrettifiedMethod(PrettifiedMethod):

    fixtures = ['local_hyphen_source_def']

    def test_prettify_method0(self):
        self.test_prettify_method('isfile',0,'not found')

    def test_prettify_method1(self):
        self.test_prettify_method('isfile_and_positive_size',1)

    def test_prettify_method2(self):
        self.test_prettify_method('METHOD_TWICE',2,'methods')

