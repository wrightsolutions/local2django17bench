from django.test import TestCase
from local2dj14.models import Source, Def

URL_COMMON0='local2'
URL_COMMON1='/local2'

class WordStem(TestCase):

    def test_wordstem(self,wordstem=None,minimum_results=5):
        if wordstem is None:
            return
        """Tests that counts url completes okay and was supplied with
        expected entry in context.
        """
        test_target = "/{0}/defnamewordstem/{1}".format(URL_COMMON0,
                                                        wordstem)
        resp = self.client.get(test_target)
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('list_of_dicts' in resp.context)
        #print resp.context['title']
        list_of_dicts = resp.context['list_of_dicts']
        len_list_of_dicts = len(list_of_dicts)
        #self.assertEqual(len_list_of_dicts,0,'No results should have been returned')
        self.assertTrue(len_list_of_dicts >= minimum_results,
                        "At least {0} results should have been returned but {1} {2}".format(
                minimum_results,len_list_of_dicts,'falls short.'))

        """
        if resp.status_code != 200:
            print resp.status_code
            raise Exception(resp)
        """

     
class DefNameWordStemStrings(WordStem):

    def test_wordstem_strings(self):
        self.test_wordstem('strings',2)

    def test_wordstem_string(self):
        self.test_wordstem('string',3)
