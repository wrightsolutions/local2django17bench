from django.test import TestCase
from local2dj14.models import Source, Def

class SourceDefCountTestInitial(TestCase):

    #fixtures = ['testdata_source_def']
    #multi_db = True

    def __init__(self, *args, **kwargs):
        super(SourceDefCountTestInitial,self).__init__(*args, **kwargs)
        #super(SourceDefCountTest,self)._fixture_setup()

    def test_source_count_ok_passed(self):
        """Tests that counts url completes okay and was supplied with
        expected entry in context.
        """
        resp = self.client.get('/local2/counts/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('source_count' in resp.context)
        self.assertTrue(resp.context['source_count'] > 0,'source_count should be zero or positive.')
        self.assertEqual(resp.context['source_count'], 3)


    def test_def_count_ok_passed(self):
        """Tests that counts url completes okay and was supplied with
        expected entry in context.
        """
        resp = self.client.get('/local2/counts/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('def_count' in resp.context)
        self.assertTrue(resp.context['def_count'] > 0,'def_count should be zero or positive.')
        self.assertEqual(resp.context['def_count'], 13)


class SourceDefCountTest(TestCase):

    fixtures = ['local_hyphen_source_def']

    def test_source_count_local_hyphen(self):
        """Tests that counts url completes okay and was supplied with
        expected entry in context.
        """
        resp = self.client.get('/local2/counts/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('source_count' in resp.context)
        self.assertEqual(resp.context['source_count'], 57)


    def test_def_count_local_hyphen(self):
        """Tests that counts url completes okay and was supplied with
        expected entry in context.
        """
        resp = self.client.get('/local2/counts/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('def_count' in resp.context)
        self.assertEqual(resp.context['def_count'], 190)
