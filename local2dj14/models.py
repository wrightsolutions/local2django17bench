from django.db import models

class Source(models.Model):

    """Populated automatically by Python and/or Django local code and not
    intended for user editing. Might be useful to allow some user annotation
    fields / comments at some point in the future.
    """

    id = models.IntegerField(primary_key=True,editable=False)
    project = models.CharField(max_length=50,db_index=True,editable=False)
    subdirpath = models.CharField(max_length=150,editable=False)
    filename = models.CharField(max_length=100,db_index=True,editable=False)
    fileext = models.CharField(max_length=15,db_index=True,editable=False)
    md5sum = models.CharField(max_length=32,editable=False)
    linecount = models.PositiveIntegerField(db_index=True,editable=False)
    defcount = models.PositiveSmallIntegerField(db_index=True,editable=False)
    sourcecode_firstline = models.CharField(max_length=130,db_index=True,editable=False)
    modified_at = models.DateTimeField(editable=False)
    sourcecode = models.TextField(editable=False)
    class Meta:
        db_table = u'source'


class Def(models.Model):

    """Populated automatically by Python and/or Django local code and not
    intended for user editing. Might be useful to allow some user annotation
    fields / comments at some point in the future.
    """

    id = models.IntegerField(primary_key=True,editable=False)
    source = models.ForeignKey(Source)
    def_name = models.CharField(max_length=100,db_index=True,editable=False)
    def_name_as_words = models.CharField(max_length=120,db_index=True,editable=False)
    def_signature_firstline = models.CharField(max_length=130,db_index=True,editable=False)
    def_signature_sourceline = models.PositiveIntegerField(editable=False)
    modified_at = models.DateTimeField(editable=False)
    class Meta:
        db_table = u'def'


class Phrase(models.Model):

    """Populated by user entry into forms for Source and Def respectively
    which allow Phrases / Keywords to be associated with saved Source / Def
    entries.
    """

    id = models.IntegerField(primary_key=True,editable=False)
    phrase = models.CharField(max_length=150,null=False,db_index=True)
    sid = models.ForeignKey(Source)
    did = models.ForeignKey(Def)
    modified_at = models.DateTimeField(editable=False)
    class Meta:
        db_table = u'phrase'


class Keyword(models.Model):

    """Populated by user entry into forms for Source and Def respectively
    which allow Phrases / Keywords to be associated with saved Source / Def
    entries.
    """

    id = models.IntegerField(primary_key=True)
    keyword = models.CharField(max_length=30,null=False,db_index=True)
    sid = models.ForeignKey(Source)
    did = models.ForeignKey(Def)
    modified_at = models.DateTimeField(editable=False)
    class Meta:
        db_table = u'keyword'
