import djapian
from local2dj14.models import Source, Def

class DefIndexer(djapian.Indexer):
    fields=["def_name"]
    tags=[
        ("def_name",  "def_name", 36),
        ("def_name_as_words",  "def_name_as_words", 26),
    ]

class SourceIndexer(djapian.Indexer):
    fields=["id","filename","sourcecode"]
    tags=[
        ("id",  "id", 6),
        ("project",  "project", 16),
        ("subdirpath",  "subdirpath", 26),
        ("filename",  "filename", 46),
        ("sourcecode",  "sourcecode", 36),
    ]

try:
    djapian.space.add_index(Def, DefIndexer, attach_as="defindexer")
except ValueError,e:
    import logging as pylog
    logger = pylog.getLogger('djapian')
    logger.warning("Ignoring Djapian add_index ValueError: {0}".format(unicode(e)))

try:
    djapian.space.add_index(Source, SourceIndexer, attach_as="sourceindexer")
except ValueError,e:
    import logging as pylog
    logger = pylog.getLogger('djapian')
    logger.warning("Ignoring Djapian add_index ValueError: {0}".format(unicode(e)))

Def.defindexer.update()
Source.sourceindexer.update()
