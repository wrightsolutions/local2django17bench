#!/usr/bin/env python

# Copyright (c) 2013, Gary Wright wrightsolutions.co.uk/contact

# Permission to use, copy, modify, and/or distribute this software
# for any purpose with or without fee is hereby granted,
# provided that the above copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARANTIES OF MERCHANTABILITY AND FITNESS.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT,
# OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
# ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE

# http://opensource.org/licenses/ISC

import ast

method_twice = ['#!/usr/bin/env python']
method_twice.append('def len_doubled(len_input):')
method_twice.append('    return 2*len(len_input)')
method_twice.append('class StringDoubler(object):')
method_twice.append('    def __init__(self,str):')
method_twice.append('        self.str = str')
method_twice.append('        self.doubled = 2*str')
method_twice.append('    def len_doubled(self):')
method_twice.append('        return len(self.doubled)')
""" METHOD_TWICE is just a string representation of a short Python script that has
the same method name occurring twice. Helpful for testing that your workings around
syntax trees are not just restricted to the first occurence of a method.
"""
METHOD_TWICE='\n'.join(method_twice)

class MethodVisitor(ast.NodeVisitor):

    def visit_Module(self, node):
        self.module_list = []
        self.method_current = None
        self.method_current_args = None
        self.method_current_lineno = None
        self.module_list_of_dicts = []
        self.generic_visit(node)

    def visit_Assign(self, node): pass

    def visit_Attribute(self, node): pass

    def visit_Call(self, node): pass

    def visit_FunctionDef(self, node):
        node_name = None
        node_args = None
        linefrom = None
        try:
            node_name = node.name
            node_args = node.args
            linefrom = node.lineno
        except AttributeError, e:
            print "node AttributeError: {0}".format(e.args[0])
        except:
            raise

        #print "Method name: {0}".format(node_name)
        cnode = None
        cnode_name = None
        for cnode in ast.iter_child_nodes(node):
            cnode_type_name = type(cnode).__name__

        lineto = None
        if cnode:
            try:
                lineto = cnode.lineno
            except AttributeError, e:
                print "node AttributeError: {0}".format(e.args[0])
            except:
                raise

        self.module_list.append(node_name)
        self.module_list_of_dicts.append({'name': node_name,
                                          'args': node_args,
                                          'linefrom': linefrom,
                                          'lineto': lineto})

        #ast.NodeVisitor.generic_visit(self, node)
        return

    def visit_Load(self, node): pass

    def visit_Return(self, node): pass

    def visit_Load(self, node): pass

