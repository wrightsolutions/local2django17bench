#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0.

""" Module for variables and methods related in some way to paths and files
"""

#from django.core.validators import RegexValidator
#from django.core.exceptions import ValidationError

#import re

__license__ = "Apache-2.0"


def source_filename(file_name,file_ext):
    """ The data model places no restriction on how many dot file extension portions
    there should be by storing filename unaltered. Example rc.new.newest.py would
    be stored as fileext='py' and filename='rc.new.newest.py'.
    So the application should include intelligence to strip off and reinstate the final
    extension as required. Hence this function.
    """
    return "{0}.{1}".format(file_name,file_ext)

