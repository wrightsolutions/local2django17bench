#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0.

""" Module for variables and methods related in some way to timezones
"""

#from django.core.validators import RegexValidator
#from django.core.exceptions import ValidationError

#import re

__license__ = "Apache-2.0"


LOCALTIME_FROZEN = None
try:
    from pytz import timezone
except:
    LOCALTIME_FROZEN = None
else:
    from datetime import datetime
    LOCALTIME_FROZEN = {'localtime_source': 'pytz',
        'localtime': timezone('Etc/UTC').localize(datetime.now()),
        'localtime_string': str(timezone('Etc/UTC').localize(datetime.now()))}

try:
    from django.utils import timezone
except:
    pass #LOCALTIME_FROZEN = None
else:
    LOCALTIME_FROZEN = {'localtime_source': 'django',
        'localtime': timezone.localtime(timezone.now()),
        'localtime_string': str(timezone.localtime(timezone.now()))}

LOCALTIME_FROZEN_STRING = ''
if LOCALTIME_FROZEN:
    LOCALTIME_FROZEN_STRING = LOCALTIME_FROZEN['localtime_string']

