#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0.

""" Module for variables and methods related in some way to http / hypertext including
internet addressing and url helpers.
"""

#from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError

import time
import re

__license__ = "Apache-2.0"

def addr_first_stripped(addr_csv,invalid_none_flag=True):
    if addr_csv:
        if addr_csv.find(',') > 1:
            addr = addr_csv.split(",")[0]
        else:
            addr = addr_csv.strip()
    else:
        addr = addr_csv

    try:
        from django.core.validators import validate_ipv4_address
        validate_ipv4_address(addr)
    except ValidationError:
        if invalid_none_flag:
            return None
        else:
            return addr

    try:
        from django.core.validators import validate_ipv6_address
        #from django.core.validators import validate_ipv46_address
        try:
            validate_ipv6_address(addr)
        except ValidationError:
            if invalid_none_flag:
                return None
    except:
        pass
    return addr

def remote_address(request,remote_from_forwarded=False):
    forwarded_for = None
    if 'HTTP_X_FORWARDED_FOR' in request.META or True:
        #forwarded_for = addr_first_stripped(request.META.get('HTTP_X_FORWARDED_FOR'))
        forwarded_for = addr_first_stripped(request.META.get('REMOTE_ADDR'))
    if forwarded_for:
        addr = forwarded_for
    else:
        addr = request.META.get('REMOTE_ADDR')
    return addr

def time_slash_product(request,remote_from_forwarded=False):
    addr = remote_address(request,remote_from_forwarded)
    #print addr
    octet_product = 0
    if '.' in addr:
        octet_array = addr.split('.')
        octet_array_penultimate = int(octet_array[-2])
        octet_array_ultimate = int(octet_array[-1])
        #print octet_array_penultimate, octet_array_ultimate
        if octet_array_penultimate > 0 and octet_array_ultimate > 0:
            octet_product = octet_array_penultimate * octet_array_ultimate
        else:
            octet_product = 6775471
    return "{0:d}{1}{2:d}".format(int(time.time()),chr(47),octet_product)

def url_with_context(request,url,drop_count=1):
    if url.startswith(chr(47)):
        return url
    url_context = chr(47).join(request.get_full_path().split(chr(47))[:-(drop_count+1)])
    return "{0}{1}{2}".format(url_context,chr(47),url)

