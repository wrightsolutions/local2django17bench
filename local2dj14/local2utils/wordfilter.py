#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0.

""" Module for variables and methods related in some way to word filtering and profanity
"""

#from django.core.validators import RegexValidator
#from django.core.exceptions import ValidationError

import re

__license__ = "Apache-2.0"


def profanities_tuple(plist_extra=None):
    plist = []
    try:
        from django.conf import settings
    except ImportError,e:
        pass
    else:
        if settings.PROFANITIES_LIST:
            plist = list(settings.PROFANITIES_LIST)
    if plist_extra:
        if len(plist_extra) > 0:
            plist.extend(plist_extra)
    return tuple(plist)

""" Two arbitrary choices for testing that would not appear in any method name """
PROFANITIES_LIST = profanities_tuple(['fartwhistle','fungibreath'])
#print PROFANITIES_LIST


def title_or_unknown(stem, tail, results):
    title = "{0} - unknown".format(stem)
    b_index = 20   # results not found for the word(s) in tail
    if results and tail:
        if len(results) > 0:
            b_index = 0   # set to 'no profanity found' at this point
            for w in tail:
                if w in PROFANITIES_LIST:
                    b_index = 50   # profanity
                    break
    if b_index > 0:
        title = "{0} - unknown".format(stem)
    else:
        title = "{0} - {1}".format(stem,tail)
    return title, b_index

