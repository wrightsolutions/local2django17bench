#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0.

""" Module for variables and methods related in some way to making source
code more presentable with highlighting / emphasis.
"""

__license__ = "Apache-2.0"

from os import linesep

from pygments.formatters import HtmlFormatter as pygHtmlFormatter
from pygments import highlight as pygHighlight
from pygments.lexers import PythonLexer, get_lexer_by_name, get_lexer_for_filename, guess_lexer

BIN_DASH='#!/bin/dash'

def string_lines(string_with_seps,l_start,l_end=None,l_sep=linesep):
    try:
        l_start = int(l_start)
    except ValueError,e:
        return None

    try:
        if l_end is None:
            string_section_list = string_with_seps.split(l_sep)[l_start:]
            string_section = l_sep.join(string_section_list)
        else:
            try:
                l_end = int(l_end)
            except ValueError,e:
                return None
            string_section_list = string_with_seps.split(l_sep)[l_start:l_end+1]
            string_section = l_sep.join(string_section_list)
    except TypeError,e:
        return None
    return string_section

def string_lines_dash(string_with_seps,l_start,l_end,l_sep=linesep):
    try:
        l_start = int(l_start)
        l_end = int(l_end)
    except ValueError,e:
        return BIN_DASH

    try:
        string_section_list = string_with_seps.split(l_sep)[l_start:l_end+1]
        string_section = l_sep.join(string_section_list)
    except TypeError,e:
        return BIN_DASH
    return string_section

def string_lines_return(string_with_seps,l_start,l_end,l_sep,error_return=BIN_DASH):
    try:
        l_start = int(l_start)
        l_end = int(l_end)
    except ValueError,e:
        return BIN_DASH

    try:
        string_section_list = string_with_seps.split(l_sep)[l_start:l_end+1]
        string_section = l_sep.join(string_section_list)
    except TypeError,e:
        return error_return
    return string_section
   

def prettify(code, lexer, formatter):
    code_pretty = pygHighlight(code, lexer, formatter)
    return code_pretty


class Prettify(object):

    def __init__(self, string_with_seps, lsep=linesep):
        self.string_with_seps = string_with_seps
        self._lsep = lsep
        self._start = None
        self._end = None
        self._error_string = None

    def get_lsep(self):
        return self._lsep

    def set_lsep(self, value):
        self._lsep = int(value)

    def del_lsep(self):
        del self._lsep

    lsep = property(get_lsep, set_lsep, del_lsep, 'line separator property.')


    def get_start(self):
        return self._start

    def set_start(self, value):
        if value:
            self._start = int(value)
        else:
            self._start = None

    def del_start(self):
        del self._start

    start = property(get_start, set_start, del_start, 'start property.')


    def get_end(self):
        return self._end

    def set_end(self, value):
        if value:
            self._end = int(value)
        else:
            self._end = None

    def del_end(self):
        del self._end

    end = property(get_end, set_end, del_end, 'end property.')


    def get_error_string(self):
        return self._error_string

    def set_error_string(self, value):
        self._error_string = value

    def del_error_string(self):
        del self._error_string

    error_string = property(get_error_string, set_error_string, del_error_string, 'error_string property.')


    def string_section_list(self):
        string_section_list = []
        try:
            if self.end is None:
                string_section_list = self.string_with_seps.split(self.lsep)[self.start:]
            else:
                try:
                    string_section_list = self.string_with_seps.split(self.lsep)[self.start:self.end+1]
                except ValueError,e:
                    #return []
                    raise
                except:
                    raise
                
        except TypeError,e:
            raise

        return string_section_list


    def split_join(self):

        try:
            l_start = int(self.start)
        except ValueError,e:
            return self.error_string

        string_section_list = []
        string_section = self.error_string
        try:
            #print self.start, self.end
            string_section_list = self.string_section_list()
            #print len(string_section_list)
        except StandardError,e:
            raise
        except:
            string_section = self.error_string
        finally:
            string_section = self.lsep.join(string_section_list)

        return string_section

    
    def prettify(self, lexer, formatter):
        if self.start:
            #print self.split_join()
            code_pretty = pygHighlight(self.split_join(), lexer, formatter)
        else:
            code_pretty = pygHighlight(self.string_with_seps, lexer, formatter)
        return code_pretty



class PrettifyShifted(Prettify):

    """ Shifted version that takes line numbers as naturally entered (in a url perhaps) and 
    applies a shift (-1) to account for Python zero indexing. 
    Example: User wants lines 38 through 44 and enters that in a url.
    In that case PrettifyShifted is going to adjust
    and give what internally is held as 37 through 43.
    """

    def string_section_list(self):
        string_section_list = []
        try:
            if self.end is None:
                string_section_list = self.string_with_seps.split(self.lsep)[self.start-1:]
            else:
                try:
                    string_section_list = self.string_with_seps.split(self.lsep)[self.start-1:self.end]
                except ValueError,e:
                    #return []
                    raise
                except:
                    raise
                
        except TypeError,e:
            raise

        return string_section_list

