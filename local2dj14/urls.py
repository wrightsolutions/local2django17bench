from django.conf.urls import patterns, url

urlpatterns = patterns('local2dj14.views',
    url(r'^$', 'home', name='local2home'),
    url(r'^counts/$', 'counts', name='local2counts'),
    url(r'^advanced/$', 'source_search_advanced', name='local2advanced'),
    url(r'^simple/$', 'source_search_simple', name='local2simple'),
    url(r'^simplelist/[-\w]+$', 'home', name='local2home_simplelist'),
    url(r'^simplelist/(?P<time>\d+)/(?P<product>\d+)/$', 'simplelist', name='simple_time_product'),
    url(r'^searchres/(?P<project>[-\w]+)/(?P<sid>\d+)/(?P<filename>\w+)/(?P<fext>\w+)$', 'local2searchres'),
    url(r'^raw/(?P<project>[-\w]+)/(?P<sid>\d+)/(?P<filename>\w+)/(?P<fext>\w+)$', 'code_as_text'),
    url(r'^codeplus/(?P<project>[-\w]+)/(?P<sid>\d+)/(?P<filename>\w+)/(?P<fext>\w+)$', 'codeplus'),
    url(r'^prettify/(?P<project>[-\w]+)/(?P<filename>\w+)/(?P<fext>\w+)/$', 'prettify_all'),
    url(r'^prettify/(?P<project>[-\w]+)/(?P<filename>\w+)/(?P<fext>\w+)/(?P<name>\w+)/$', 'prettify_method'),
    url(r'^prettify/(?P<project>[-\w]+)/(?P<filename>\w+)/(?P<fext>\w+)/(?P<l_start>\d+)/(?P<l_end>\d+)/$', 'prettify_section'),
    url(r'^defnameword/(?P<queryword>[-\w]+)$', 'defnameword'),
    url(r'^defnamewordstem/(?P<queryword>[-\w]+)$', 'def_name_word_stem'),
    url(r'^intruder$', 'jresponse', {'seed': 'intruder'}, name='intruder'),
    url(r'^doris$', 'jresponse', {'seed': 'doris'}, name='doris'),
    url(r'^lettuce$', 'jresponse', {'seed': 'lettuce'}, name='lettuce'),
    # Examples:
    # url(r'^$', 'local2django17bench.views.home', name='home'),
    # url(r'^local2django17bench/', include('local2django17bench.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)

