from django import forms

class SearchSimpleForm(forms.Form):
    source_query = forms.CharField(max_length=60)
