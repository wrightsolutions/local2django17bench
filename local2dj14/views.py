from django.http import HttpResponse
from django.shortcuts import render
import models
from django.template import RequestContext
from django.utils.safestring import SafeString
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
#import local2dj14.local2utils as l2u
from local2dj14.local2utils import hyper as hyp
from local2dj14.local2utils import pathandfile as paf
from local2dj14.local2utils import wordfilter as wordfil
from local2dj14.local2utils import zonehelpers as zhelp
from local2dj14.local2utils import prettify as pre
from local2dj14.forms import SearchSimpleForm
from pygments.lexers import PythonLexer, get_lexer_by_name, get_lexer_for_filename, guess_lexer
from pygments.formatters import HtmlFormatter as pygHtmlFormatter

import re
from time import sleep

import djapian
from xapian import DatabaseLockError as xapDbLockError
try:
    djapian.load_indexes()
except xapDbLockError:
    import logging as pylog
    logger = pylog.getLogger('djapian')
    sleep(0.5)
    logger.warning('Benchmark sleep() invoked for Djapian')
    djapian.load_indexes()

BIN_DASH='#!/bin/dash'
re_prettify_k_def = re.compile(r'<span class="k">def</span>')

def counts_context():
    source_count = 0
    def_count = 0

    try:
        source_count = models.Source.objects.count()
        def_count = models.Def.objects.count()
    except ValueError,e:
        import logging as pylog
        logger = pylog.getLogger('djapian')
        logger.warning("Ignoring Djapian ValueError: {0}".format(unicode(e)))

    context = {'localtime_frozen_string': zhelp.LOCALTIME_FROZEN_STRING,
               'localtime_frozen_source': zhelp.LOCALTIME_FROZEN['localtime_source'],
               'source_count': source_count,
               'def_count': def_count}
    return context

def counts(request):
    return render(request,'counts_section.html',counts_context())

def home(request):
    return render(request,'base_paragraph.html',
                  {'title': 'Home', 'para_to_p_wrap': 'Welcome to local2 source search system.'})

def code_as_text(request,project,sid,filename,fext='py'):
    source = models.Source.objects.get(id=sid)
    context = {'sourcecode': source.sourcecode,
               'sourcecode_as_lines': source.sourcecode.splitlines(),
               'linecount': source.linecount}
    return render(request,'base_sourcecode.html',context)

def codeplus(request,project,sid,filename,fext='py'):
    title = "Code - {0}.{1}".format(filename,fext)
    source = models.Source.objects.select_related().get(id=sid)
    defs_all = source.def_set.all()
    context = {'sourcecode': source.sourcecode,
               'linecount': source.linecount,
               'defcount': source.defcount,
               'defs_all': defs_all,
               'defs_all_sorted': sorted(defs_all,key=lambda x: x.def_signature_sourceline),
               'title': title}
    return render(request,'sourcecode_plus.html',context)

def prettify_all(request,project,filename,fext='py'):
    title = "Code - {0}.{1}".format(filename,fext)
    fname = paf.source_filename(filename,fext)
    source = models.Source.objects.select_related().get(project=project,filename=fname,fileext=fext)
    defs_all = source.def_set.all()

    from pygments import highlight as pygHighlight
    #lexer = get_lexer_by_name(block['class'])
    formatter = pygHtmlFormatter(linenos='table',cssclass='prettify')
    #print pygHtmlFormatter.get_style_defs(formatter,'.highlight')
    sourcecode_pre = pygHighlight(source.sourcecode, PythonLexer(), formatter)
    sourcecode_pre = SafeString(sourcecode_pre)   # deliberate
    context = RequestContext(request, {'sourcecode': source.sourcecode,
               'sourcecode_pre': sourcecode_pre,
               'linecount': source.linecount,
               'defcount': source.defcount,
               'defs_all': defs_all,
               'defs_all_sorted': sorted(defs_all,key=lambda x: x.def_signature_sourceline),
               'title': title})
    #return render(request,'sourcecode_prettify_all_detailed.html',context)
    return render(request,'sourcecode_prettify.html',context)

def prettify_method(request,project,filename,fext,name):
    title = "Code - {0}.{1} - method".format(filename,fext)
    if fext == 'py' and name == 'METHOD_TWICE':
        print "{0:s} {1:s} {2:s} {3:s}".format(project, filename, fext, name)
        # Could include fname == 'rc.py' in the if test also
    fname = paf.source_filename(filename,fext)
    source = models.Source.objects.select_related().get(project=project,filename=fname,fileext=fext)
    defs_all = source.def_set.all()

    from ast import parse as astparse
    from local2dj14.local2utils import astlocal as astl
    if name == 'METHOD_TWICE':
        preshift = pre.PrettifyShifted(astl.METHOD_TWICE,'\n')
        st = astparse(astl.METHOD_TWICE)
        name = 'len_doubled'
    else:
        preshift = pre.PrettifyShifted(source.sourcecode,'\n')
        st = astparse(source.sourcecode)

    mv = astl.MethodVisitor()   # astlocal
    mv.visit(st)

    formatter = pygHtmlFormatter(linenos='table',cssclass='prettify')

    sourcecode_pre = ''
    def_prettified_count = 0
    if name in mv.module_list:
        method_index_list = []
        """ The for loops that follow were introduced to deal with the fact that in some
        files, particularly those that follow oo / use classes, we may well have the same
        method name appearing many times. """
        for ind,method in enumerate(mv.module_list):
            if method == name:
                method_index_list.append(ind)
        for name_index in method_index_list:
            method_dict = mv.module_list_of_dicts[name_index]
            l_start = method_dict['linefrom']
            l_end = method_dict['lineto']
            #print l_start, l_end
            preshift.start = l_start
            preshift.end = l_end
            sourcecode_pre += preshift.prettify(PythonLexer(stripnl=False,ensurenl=True), formatter)
            #def_prettified_count += 1

        def_prettified_count = len(re_prettify_k_def.findall(sourcecode_pre))
        if def_prettified_count > 1:
            title = "{0}s ({1} matches)".format(title,def_prettified_count)

    else:
        l_start = 1
        l_end = None
        title = "Code - {0}.{1} - method not found".format(filename,fext)
        preshift.start = l_start
        preshift.end = l_end
        sourcecode_pre = preshift.prettify(PythonLexer(stripnl=False,ensurenl=True), formatter)
        def_prettified_count = 0

    if sourcecode_pre:
        sourcecode_pre = SafeString(sourcecode_pre)   # deliberate
    else:
        sourcecode_pre = BIN_DASH
    start_end_tuple_list = []
    footer_list = ["Methods Prettified: {0:d}".format(def_prettified_count)]
    context = RequestContext(request, {'sourcecode_pre': sourcecode_pre,
               'title': title,
               'def_prettified_count': def_prettified_count,   # tests
               'start_end_tuple_list': start_end_tuple_list,   # tests
               'footer_list': footer_list})
    return render(request,'sourcecode_prettify_with_footer.html',context)

def prettify_section(request,project,filename,fext,l_start,l_end):
    title = "Code - {0}.{1} - section".format(filename,fext)
    fname = paf.source_filename(filename,fext)
    source = models.Source.objects.select_related().get(project=project,filename=fname,fileext=fext)
    defs_all = source.def_set.all()

    #lexer = get_lexer_by_name(block['class'])
    formatter = pygHtmlFormatter(linenos='table',cssclass='prettify')
    #formatter = pygHtmlFormatter(cssclass='prettify')   # template sourcecode_prettify_enclosed.html
    #print pygHtmlFormatter.get_style_defs(formatter,'.highlight')

    preshift = pre.PrettifyShifted(source.sourcecode,'\n')
    preshift.start = l_start
    preshift.end = l_end
    sourcecode_pre = preshift.prettify(PythonLexer(stripnl=False,ensurenl=True), formatter)
    if sourcecode_pre:
        sourcecode_pre = SafeString(sourcecode_pre)   # deliberate
    else:
        sourcecode_pre = BIN_DASH
    context = RequestContext(request, {'sourcecode_pre': sourcecode_pre,
               'title': title})
    return render(request,'sourcecode_prettify.html',context)

def defnameword(request,queryword):
    title_stem = "Code Search"
    result_objects = models.Def.defindexer.search(queryword)
    title, b_index = wordfil.title_or_unknown(title_stem,queryword,result_objects)
    """ Here r.instance gets you a matching Def instance (a full Def object)
    from which you should then select variables as appropriate """
    results = [ r.instance.def_name for r in result_objects ]
    context = {'title': title,
               'results': results}
    return render(request,'base_results.html',context)

def def_name_word_stem(request,queryword):
    list_of_dicts = []
    result_objects = models.Def.defindexer.search('def_name_as_words:{0}'.format(queryword))

    title, b_index = wordfil.title_or_unknown("Code Search",queryword,result_objects)
    if result_objects:
        #print result_objects[0].__class__.__name__)
        """ Djapian 'Hit' objects property tags points to a dictionary of the tags for that result Hit """
        list_of_dicts = [ h.tags for h in result_objects ]

    #print len(list_of_dicts)
    context = {'title': title,
               'footer_list': [b_index],
               'list_of_dicts': list_of_dicts}
    return render(request,'base_results_def_name.html',context)

def source_search_simple(request):
    title = "Code Search"

    if request.method == 'POST': # If the form has been submitted...
        form = SearchSimpleForm(request.POST) # A form bound to the POST data
        #print 'form populated.'
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            return HttpResponseRedirect('/home/')
    else:
        form = SearchSimpleForm()
        url_without_context = "simplelist/{0}/".format(hyp.time_slash_product(request,False))
        form_action = hyp.url_with_context(request,url_without_context)
        #print form_action
        
    context = RequestContext(request, {'form': form,
                                       'form_action': form_action,
                                       'form_action_method': 'POST',
                                       'title': title})
    return render(request,'base_form.html',context)

def simplelist(request,time,product):
    title_stem = "Code Search Simple"
    result_objects = None
    list_of_dicts = []
    source_query = None
    if request.method == 'POST':
        source_query = request.POST.get('source_query',None)
        if source_query:
            result_objects = models.Source.sourceindexer.search('sourcecode:{0}'.format(source_query))
    #print source_query
    #title = source_query
    title, b_index = wordfil.title_or_unknown(title_stem,source_query,result_objects)
    if result_objects:
        #print result_objects[0].__class__.__name__)
        """ Djapian 'Hit' objects property tags points to a dictionary of the tags for that result Hit """
        list_of_dicts = [ h.tags for h in result_objects ]
    context = {'title': title,
               'list_of_dicts': list_of_dicts}
    return render(request,'base_results_source.html',context)

def jresponse(request,seed):
    punchline = "Knock Knock What?"
    if seed == 'intruder':
        punchline = 'Window'
    elif seed == 'doris':
        punchline = "Doris locked, that's why I'm knocking!"
    elif seed == 'lettuce':
        punchline = "Lettuce in and you'll find out!"
    return HttpResponse(punchline)

