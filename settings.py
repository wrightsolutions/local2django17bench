# Django settings for benchmark project.
import os

try:                                                                                                                                                         
    from settings_langplus import *     # preferred to less effective variant: import settings_langplus
    print(TIME_ZONE)
    #from settings_langplus import MIDDLEWARE_CLASSES
except ImportError, e:
    print 'Unable to head import settings_langplus.py from within settings.py:', e
    """ By manually setting False on next two lines, we ensure
    that debug can only ever be enabled by a present and
    correct settings.py """
    DEBUG = False
    TEMPLATE_DEBUG = DEBUG

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

SECRET_KEY = '8xx8(zz%0tz#3k3gr9$y7ymh36=n4)4s4fy(lki-k$drie0-9!'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        #'TIMEOUT': 60,
        #'OPTIONS': {
        #    'MAX_ENTRIES': 1000
        #},
        #'KEY_PREFIX': '',
        #'LOCATION': [
        #    '111.222.111.222:11211',
        #    '111.222.111.233:11211',
        #    '111.222.111.244:11214',
        #],
    }
}


try:
    from settings_final import *     # preferred to less effective variant: import settings_final
    print(DATABASES['default'])
    print(DEBUG,TEMPLATE_DEBUG)
    #from settings_final import DEBUG,TEMPLATE_DEBUG
except ImportError, e:
    print 'Unable to tail import settings_final.py from within settings.py:', e
    """ By manually setting False on next two lines, we ensure
    that debug can only ever be enabled by a present and
    correct settings.py """
    DEBUG = False
    TEMPLATE_DEBUG = DEBUG

